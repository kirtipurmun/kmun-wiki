# Kirtipur Municipality Services Knowledge Base
## A knowledge base for Kirtipur Municipality's software systems.
This repo holds the Docker configuration for the web application knowlege base that is used by Kirtipur Municipality. This knowledge base contains the implementation details of web services that the municipality uses internally and externally. User rights are managed by the IT department, so please request access from the IT staff at Kirtipur Municipality if you are working on any of these systems.

## WikiJS Service (This repository)
This repository is used to automate the deployment of the Kirtipur Municipality Services Knowledge Base. It uses [Docker](https://www.docker.com/) to host an instance of the [official WikiJS Docker container](https://hub.docker.com/r/requarks/wiki).

### Configuration
Running this instance makes use of a `.env` file containing the following environment secrets:

```
POSTGRES_USER=someusername
POSTGRES_PASSWORD=somepassword
```

You should set these along the standard SQL username guidelines [enforced by PostgreSQL](https://www.postgresql.org/docs/8.0/user-manag.html#DATABASE-USERS), and a secure password to match.

### Starting the WikiJS service
Once a `.env` file containing these variables is created, you may launch the service by using the Docker Compose app:

```
docker-compose up -d
```

You may observe the output of this process by running the Docker Compose logs command, following the output as it is streamed to the host:

```
docker-compose logs -f
```

### User management
WikiJS uses its own [user permissions system](https://docs.requarks.io/groups), which can be configured to allow or disallow access publically, or to a subset of user accounts.

### Data storage
There is a separate _private_ `git` repository that we use to store the contents of this knowledgebase, synchronised using the `git` module of WikiJS.

Using this in conjunction with BitBucket will require SSH public key configuration.